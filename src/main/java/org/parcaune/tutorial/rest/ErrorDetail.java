package org.parcaune.tutorial.rest;

import java.io.Serializable;

public class ErrorDetail implements Serializable {

	private static final long serialVersionUID = 1L;
	private String context;
	private String message;

	public ErrorDetail(String message) {
		super();
		this.message = message;
	}

	public ErrorDetail(String context, String message) {
		super();
		this.context = context;
		this.message = message;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return " {context:'" + context + "', message:'" + message + "'}";
	}

}
