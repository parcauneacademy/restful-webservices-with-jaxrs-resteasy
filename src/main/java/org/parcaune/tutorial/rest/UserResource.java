package org.parcaune.tutorial.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * User resource
 */
@Path("users")
public class UserResource {

	// Base de donnees temporaire
	static Map<String, User> users = new HashMap<String, User>();

	static {
		System.out.println("Initializing the database...");
		users.put("1", new User("1", "Max", "Doe"));
		users.put("2", new User("2", "Au", "Pair"));
	}


	@GET
	@Path("/hello")
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHello() {
		return "User Management Application By Nick Wansi";
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getAllUsers() {
		System.out.println("Getting all Users...");

		ArrayList<User> list = new ArrayList<User>(users.values());
		return list;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserById(@PathParam("id") String id) {
		System.out.println("Getting User by ID: " + id);

		User user = users.get(id);
		if (user != null) {
			return Response.ok(user).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("User with id %s not found", id))).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(User user) {
		System.out.println("Adding User: " + user);

		if (user.getId() != null && !user.getId().isEmpty()) {
			return Response.status(Status.NOT_ACCEPTABLE)
					.entity(new ErrorDetail(String.format("User with id %s already exists", user.getId()))).build();
		}
		user.setId(UUID.randomUUID().toString());
		users.put(user.getId(), user);
		return Response.ok(user).build();
	}

	@PUT
	@Path("/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("id") String id, User user) {
		System.out.println("Updating User with ID:  "+ id);

		User old = users.get(id);
		if (old != null) {
			users.put(id, user);
			return Response.ok(user).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("User with ID: %s does NOT exist...", id))).build();
	}

	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUserById(@PathParam("id") String id) {
		System.out.println("Deleting User with ID: "+ id);

		User user = users.remove(id);
		if (user != null) {
			return Response.ok(user).build();
		}
		return Response.status(Status.NOT_FOUND)
				.entity(new ErrorDetail(String.format("User with ID: %s does NOT exist...", id))).build();
	}
}
